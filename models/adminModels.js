const mongoose = require('mongoose')
const validator = require('validator')

const adminSchema = new mongoose.Schema({
    name: {
        type:String,
        required:[true, 'Please tell your name!'], 
    },
    email:{
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo:{
        type: String,
        default: 'default.jpg',
    },
    role: {
        type:String,
        enum: ['user', 'sme', 'pharmacist','admin'],
        default:'user',
    },

    contact:{
        type:String,
        required: [true, "Please provide a contact number"],
        minlenght:8,
        select:false,

    },
    password:{
        type:String,
        required: [true, 'Please provide a password'],
        minlenght:8,
        select:false,

    },
    active:{
        type:Boolean,
        default:true,
        select:false,
    },
})

const Admin = mongoose.model('Admin', adminSchema)
module.exports = Admin